package com.kshrd.articles.configuration;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

@Configuration
@MapperScan("com.kshrd.articles.repositories")
public class MyBatisConfiguration {

    private DataSource ds;
    @Autowired
    public void setDs(DataSource ds) {
        this.ds = ds;
    }

    @Bean
    public SqlSessionFactoryBean sqlSessionFactoryBean() {
        SqlSessionFactoryBean sql=new SqlSessionFactoryBean();
        sql.setDataSource(ds);
        return sql;
    }

    @Bean
    public DataSourceTransactionManager dataSourceTransactionManager() {
        return new DataSourceTransactionManager(ds);
    }
}
