package com.kshrd.articles.controllers;

import com.kshrd.articles.models.Categories;
import com.kshrd.articles.services.CategoryServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class CategoryController {
    private CategoryServices cs;
    private Integer id=1;

    @Autowired
    public void setCs(CategoryServices cs) {
        this.cs = cs;
    }


    @GetMapping("/category")
    public String addCategory(ModelMap model) {
        model.addAttribute("cat", new Categories());
        return "category/add";
    }

    @PostMapping("/add/submit/category")
    public String SaveAddCategory(ModelMap model, @Valid @ModelAttribute Categories c, BindingResult result) {
        if(result.hasErrors()) {
            model.addAttribute("cat", c);
            model.addAttribute("error", result.hasErrors());
            return "category/add";
        }
        c.setId(id);

        cs.addCategory(c);
        id++;
        System.out.println(c.getName());
        return "redirect:/allcategory";
    }

    @GetMapping({"/allcategory",""})
    public String listAllCategory(ModelMap model) {
        model.addAttribute("cat", cs.findAll());
        return "category/index";
    }

    @GetMapping("/update/category/{id}")
    public String updateCategory(ModelMap model, @PathVariable int id) {
        model.addAttribute("cat", cs.findRecord(id));
        return "category/update";
    }

    @PostMapping("/submit/update/category")
    public String submitUpdateCategory(ModelMap model,@ModelAttribute Categories c, BindingResult result) {
        System.out.println(c.getId()+c.getName());
        if(result.hasErrors()) {
            model.addAttribute("cat", c);
            model.addAttribute("error", result.hasErrors());
            return "category/add";
        }
        if(cs.updateRecord(c)) {
            System.out.println("Record was Update----!");
        }
        return "redirect:/allcategory";
    }

    @GetMapping("/delete/category/{id}")
    public String deleteCategory(@PathVariable int id) {
        cs.deleteRecord(id);
        return "redirect:/allcategory";
    }
}
