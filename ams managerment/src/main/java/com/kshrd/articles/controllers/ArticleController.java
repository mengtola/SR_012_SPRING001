package com.kshrd.articles.controllers;

import com.kshrd.articles.models.Articles;

import com.kshrd.articles.services.ArticleService;
import com.kshrd.articles.services.CategoryServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Controller
public class ArticleController {
    String filename=null;
    private CategoryServices categoryService;
    private ArticleService articleService;

    @Autowired
    public void setArticleService(ArticleService articleService) {
        this.articleService = articleService;
    }

    @Autowired
    public void setCategoryService(CategoryServices categoryService) {
        this.categoryService = categoryService;
    }

    @Value("${file.upload.server.path}")
    String serverPath;

    @GetMapping("/home")
    public String findAll(ModelMap m ){
        List <Articles> arts= articleService.findAllRecords();
        m.addAttribute("articles",arts);
        return "ListArticle";
    }

    @GetMapping("/add")
    public String add(ModelMap m ){
        m.addAttribute("article",new Articles());
        m.addAttribute("categories",categoryService.findAll());
        m.addAttribute("formAdd",true);
        return "Add";
    }

    @PostMapping("/add")
    public String saveArticle(@RequestParam("thumb") MultipartFile thumbnail,@Valid @ModelAttribute Articles a, BindingResult result,ModelMap m ){
        if(result.hasErrors()) {
            m.addAttribute("article", "article");
            m.addAttribute("categories",categoryService.findAll());
            m.addAttribute("formAdd", true);
            return "Add";
        }
        if (thumbnail.isEmpty()) {
            return "Add";
        }else {
            try {
                filename= UUID.randomUUID().toString()+"."+ thumbnail.getOriginalFilename().substring(thumbnail.getOriginalFilename().lastIndexOf(".")+1);
                Files.copy(thumbnail.getInputStream(), Paths.get(serverPath,filename ));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        int id=1;
        a.setId(id);
        a.setThumbnail("/image/"+filename);
        a.setCreatedate(new Date().toString());
        a.setCategory(categoryService.findRecord(a.getCategory().getId()));
        System.out.println("cat id "+categoryService.findRecord(a.getCategory().getId()));
        articleService.addNewArticle(a);
        id++;
        return "redirect:/home";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Integer id){
        articleService.delete(id);
        return "redirect:/home";
    }

    @GetMapping("/update/{id}")
    public String update(ModelMap m ,@PathVariable Integer id){
        Articles articles =articleService.findRecord(id);
        m.addAttribute("article",articles);
        m.addAttribute("categories",categoryService.findAll());
        m.addAttribute("formAdd",false);
        return "Add";
    }

    @PostMapping("/update")
    public String saveUpdate(@ModelAttribute Articles a){
        a.setCreatedate(new Date().toString());
        articleService.update(a);
        return "redirect:/home";
    }
}
