package com.kshrd.articles.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.annotation.MultipartConfig;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

@Controller
public class FileUploadController {
    @GetMapping("/upload")
    public String upload(){
        return "upload";
    }

    @PostMapping("/upload")
//    Single upload file
//    public String saveFile(@RequestParam ("file") MultipartFile file){
//
//        String serverPath ="/Users/Anonymous/Desktop/SR>";
//        if (!file.isEmpty()){
//            String filename= UUID.randomUUID().toString()+"."+ file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1);
//            try {
//                Files.copy(file.getInputStream(), Paths.get(serverPath,filename ));
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//
//        return "redirect:/upload";
//    }

//    Multi upload

    public String saveFile(@RequestParam ("files") List<MultipartFile> files){

        String serverPath ="C:\\Users\\Anonymous\\Desktop\\SR";
        if (!files.isEmpty()){
            files.forEach(file ->{
                String filename= UUID.randomUUID().toString()+"."+ file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1);
                try {
                    Files.copy(file.getInputStream(), Paths.get(serverPath,filename ));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
        return "redirect:/upload";
    }
}
