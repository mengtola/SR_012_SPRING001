package com.kshrd.articles.models;

import ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.validation.constraints.NotNull;

public class Articles {
    private  int id;
//    @NotBlank
    private  String title;
//    @NotBlank
    private  String description;
//    @NotBlank
    private  Categories categories;
    private  String author;
    private  String thumbnail;
    private  String createdate;

    public Articles(){}
    public Articles(int id, String title, String description, Categories category,String author,String thumbnail, String createdate) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.categories=category;
        this.author = author;
        this.thumbnail=thumbnail;
        this.createdate=createdate;
    }

    @Override
    public String toString() {
        return "Articles{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", categories=" + categories +
                ", author='" + author + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                ", createdate='" + createdate + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Categories getCategory() {
        return categories;
    }

    public void setCategory(Categories category) {
        this.categories = category;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getCreatedate() {
        return createdate;
    }

    public void setCreatedate(String createdate) {
        this.createdate = createdate;
    }
}
