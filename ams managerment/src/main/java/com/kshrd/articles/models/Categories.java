package com.kshrd.articles.models;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

public class Categories {
    @NotNull
    private int id;
    @NotBlank
    private String name;
    public Categories() {}
    public Categories(int id, String name) {
        super();
        this.id = id;
        this.name = name;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Categories{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
