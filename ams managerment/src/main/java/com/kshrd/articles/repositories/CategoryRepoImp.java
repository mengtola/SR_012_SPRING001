package com.kshrd.articles.repositories;

import com.kshrd.articles.models.Categories;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CategoryRepoImp implements CategoryRepo{
    private List<Categories> categories=new ArrayList<>();
    public CategoryRepoImp (){
        categories.add(new Categories(1,"Spring"));
        categories.add(new Categories(2,"Java"));
        categories.add(new Categories(3,"Web"));
        categories.add(new Categories(4,"Korean"));
        categories.add(new Categories(5,"jQuery"));
        categories.add(new Categories(6,"AJAX"));
        categories.add(new Categories(7,"JSON"));
    }

    @Override
    public void addCategory(Categories c) {
        categories.add(c);
    }

    @Override
    public Categories findRecord(Integer id) {
        for (Categories category : categories){
            if(category.getId()==id){
                return category;
            }
        }
        return null;
    }

    @Override
    public List<Categories> findAll() {
        return categories;
    }

    @Override
    public boolean updateCategory(Categories c) {
        for(int i=0;i<categories.size();i++){
            if(categories.get(i).getId()==c.getId()){
                categories.get(i).setName(c.getName());
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean deleteRecord(Integer id) {
        for (Categories category : categories) {
            if (category.getId() == id)
                categories.remove(id);
            return true;
        }
        return false;
    }
}
