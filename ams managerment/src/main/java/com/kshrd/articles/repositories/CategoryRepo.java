package com.kshrd.articles.repositories;

import com.kshrd.articles.models.Categories;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface CategoryRepo {
    @Insert("INSERT INTO tb_categories (id,name) VALUES (#{id},#{name})")
    public void addCategory(Categories c);

    @Select("SELECT name FROM tb_categories WHERE id = #{id}")
    public Categories findRecord(Integer id);

    @Select("SELECT id ,name FROM tb_categories")
    List<Categories> findAll();

    @Update("UPDATE tb_categories SET name= #{name} WHERE id=#{id}")
    public boolean updateCategory(Categories c);

    @Delete("DELETE FROM tb_categories WHERE id=#{id}")
    public boolean deleteRecord(Integer id);
}