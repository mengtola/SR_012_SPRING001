package com.kshrd.articles.repositories;

import com.kshrd.articles.models.Articles;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleRepo {

    @Insert("INSERT INTO tb_articles(title,category_id,author,description,thumbnail,create_date) VALUES (#{title},#{categories.id},#{author},#{description},#{thumbnail},#{createdate})")
    void addNewArticle(Articles articles);

    @Select("SELECT a.id,a.title,c.name cate_name,a.author,a.description,a.thumbnail,a.create_date FROM tb_articles a INNER JOIN tb_categories c on c.id=a.id WHERE a.category_id=#{id}")
    @Results({
            @Result(property="category.id",column="category_id"),
            @Result(property="category.name",column="cate_name"),
            @Result(property="createdate",column="create_date")
    })
    public Articles findRecord(int id);

    @Select("SELECT a.id,a.title,c.name cate_name,a.author,a.description,a.thumbnail,a.create_date FROM tb_articles a INNER JOIN tb_categories c on a.category_id=c.id")
    @Results({
            @Result(property="categories.id",column="category_id"),
            @Result(property="categories.name",column="cate_name"),
            @Result(property="createdate",column="create_date")
    })
    List <Articles> findAllRecords();

    @Delete("DELETE FROM tb_articles WHERE id=#{id}")
    void delete(int id);

    @Update("UPDATE tb_articles SET title=#{title}, category_id=#{categories.id},author=#{author},description=#{description},thumbnail=#{thumbnail},create_date=NOW() WHERE id=#{id}")
    void update(Articles article);
}
