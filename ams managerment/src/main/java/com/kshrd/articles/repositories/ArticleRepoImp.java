package com.kshrd.articles.repositories;

import com.github.javafaker.Faker;
import com.kshrd.articles.models.Articles;
import com.kshrd.articles.models.Categories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//@Repository
public class ArticleRepoImp implements ArticleRepo {
    private List <Articles> articles=new ArrayList<>();
    public ArticleRepoImp() {
        Faker f=new Faker();
        articles.add(new Articles(1,f.book().title(),f.artist().name(),new Categories(1,"Spring"),f.book().author(),f.avatar().image(),new Date().toString()));
        articles.add(new Articles(2,f.book().title(),f.artist().name(),new Categories(2,"WEB"),f.book().author(),f.avatar().image(),new Date().toString()));
        articles.add(new Articles(3,f.book().title(),f.artist().name(),new Categories(3,"MVC"),f.book().author(),f.avatar().image(),new Date().toString()));
        articles.add(new Articles(4,f.book().title(),f.artist().name(),new Categories(4,"MODEL"),f.book().author(),f.avatar().image(),new Date().toString()));
        articles.add(new Articles(5,f.book().title(),f.artist().name(),new Categories(5,"JAVA"),f.book().author(),f.avatar().image(),new Date().toString()));
        articles.add(new Articles(6,f.book().title(),f.artist().name(),new Categories(6,"POJOs"),f.book().author(),f.avatar().image(),new Date().toString()));
        articles.add(new Articles(7,f.book().title(),f.artist().name(),new Categories(7,"KOREA"),f.book().author(),f.avatar().image(),new Date().toString()));
        articles.add(new Articles(8,f.book().title(),f.artist().name(),new Categories(8,"jQuery"),f.book().author(),f.avatar().image(),new Date().toString()));
    }
    public List<Articles> getArticles() {
        return articles;
    }

    public void setArticles(List<Articles> articles) {
        this.articles = articles;
    }

    @Override
    public void addNewArticle(Articles article) {
        articles.add(article);
    }

    @Override
    public Articles findRecord(int id) {
        for (Articles article : articles)
            if (article.getId()==id){
                return article;
            }
            return null;
    }
    @Override
    public List<Articles> findAllRecords() {
        return articles;
    }

    @Override
    public void delete(int id) {
        for (Articles article : articles) {
            if (article.getId() == id)
                articles.remove(article);
                return;
        }
    }

    @Override
    public void update(Articles article) {
        for(int i=0;i<articles.size();i++){
            if(articles.get(i).getId()==article.getId()){
                articles.get(i).setTitle(article.getTitle());
                articles.get(i).setDescription(article.getDescription());
                articles.get(i).setAuthor(article.getAuthor());
                return;
            }
        }
    }
}
