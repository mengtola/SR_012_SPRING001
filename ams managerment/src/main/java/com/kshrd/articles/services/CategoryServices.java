package com.kshrd.articles.services;

import com.kshrd.articles.models.Categories;

import java.util.List;

public interface CategoryServices {
    void addCategory(Categories category);
    List<Categories> findAll();
    Categories findRecord(Integer id);
    boolean updateRecord(Categories category);
    boolean deleteRecord(Integer id);
}
