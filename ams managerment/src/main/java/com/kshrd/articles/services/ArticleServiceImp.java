package com.kshrd.articles.services;

import com.kshrd.articles.models.Articles;
import com.kshrd.articles.repositories.ArticleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServiceImp implements ArticleService {

    private ArticleRepo articleRepo;
    @Autowired
    public void setArticleRepo(ArticleRepo articleRepo) {
        this.articleRepo = articleRepo;
    }

    @Override
    public void addNewArticle(Articles articles) {
        articleRepo.addNewArticle(articles);
    }

    @Override
    public Articles findRecord(int id) {
        return articleRepo.findRecord(id);
    }

    @Override
    public List<Articles> findAllRecords() {
        return articleRepo.findAllRecords();
    }

    @Override
    public void delete(int id) {
        articleRepo.delete(id);
    }

    @Override
    public void update(Articles article) {
        articleRepo.update(article);
    }
}
