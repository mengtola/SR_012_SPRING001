package com.kshrd.articles.services;

import com.kshrd.articles.models.Categories;
import com.kshrd.articles.repositories.CategoryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class CategoryServiceImp  implements CategoryServices {
    @Autowired
    private CategoryRepo categoryRepo;

//    @Autowired
//    public void setCategoryRepo(CategoryRepo categoryRepo) {
//        this.categoryRepo = categoryRepo;
//    }

    @Override
    public void addCategory(Categories category) {
        categoryRepo.addCategory(category);
    }

    @Override
    public Categories findRecord(Integer id) {
        return categoryRepo.findRecord(id);
    }

    @Override
    public List<Categories> findAll() {
        return categoryRepo.findAll();
    }

    @Override
    public boolean updateRecord(Categories c) {
        return categoryRepo.updateCategory(c);
    }

    @Override
    public boolean deleteRecord(Integer id) {
        return categoryRepo.deleteRecord(id);
    }
}