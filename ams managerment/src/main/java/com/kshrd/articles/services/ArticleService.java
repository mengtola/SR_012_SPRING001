package com.kshrd.articles.services;

import com.kshrd.articles.models.Articles;
import org.springframework.stereotype.Service;

import java.util.List;
//@Service
public interface ArticleService {
    void addNewArticle(Articles articles);
    Articles findRecord(int id);
    List<Articles> findAllRecords();
    void delete(int id);
    void update(Articles articles);
}
